package com.example.curzonnumbers;

class CurzonNumbers {
    static void checkIfCurzonNumber(long N) {
        double num1, num2;
        // Find 2^N + 1
        num1 = Math.pow(2, N) + 1;
        // Find 2*N + 1
        num2 = 2 * N + 1;
        // Check for divisibility
        if (num1 % num2 == 0)
            System.out.println("Yes number " + N + " is a Curzon Number.");
        else
            System.out.println("No number " + N + " is not a curzon number.");
    }

    // Driver code
    public static void main(String[] args)
    {
        long N = 5;
        checkIfCurzonNumber(N);

        N = 10;
        checkIfCurzonNumber(N);
    }
}
